const table = document.querySelector(".mytable > tbody");
const totalInfo = document.querySelector(".total-info");
const pageButtonWrapper = document.querySelector(".page-buttons-wrapper");
const previousButton = document.querySelector(".previous-button");
const nextButton = document.querySelector(".next-button");
const sortFields = document.querySelectorAll(".sort-field");
var sortFieldText = "Name";
var order = 1;
var numberOfPage;
const tableData = [];
var dataToShow = [];
var pageLength = 10;
var pageIndex = 1;

fetch("data.csv")
  .then((response) => response.text())
  .then((data) => {
    const rows = data.split("\n");
    rows.shift();
    rows.forEach((row) => {
      // console.log(row);
      const cells = row.split('","');
      // console.log(cells);
      var newRow = {
        name: cells[0].replace(/"/g, ""),
        position: cells[1].replace(/"/g, ""),
        office: cells[2].replace(/"/g, ""),
        age: cells[3].replace(/"/g, ""),
        startDate: cells[4].replace(/"/g, ""),
        salary: cells[5].replace(/"/g, ""),
      };
      // console.log(newRow);
      tableData.push(newRow);
    });
    dataToShow = tableData.map((obj) => ({ ...obj }));
    totalInfo.innerHTML = dataToShow.length;
    showintable();
    var beforeElement = sortFields[0].querySelector(".ascending");
    beforeElement.style.opacity = "0.6";
    generatePageButton();
    setSortingColumnColor();
  });
const firstInfo = document.querySelector(".first-info");
const lastInfo = document.querySelector(".last-info");
const totalFilteredInfo = document.querySelector(".total-info-filtered");
function showintable() {
  table.innerHTML = "";
  firstInfo.innerHTML = (pageIndex - 1) * pageLength + 1;
  lastInfo.innerHTML =
    pageIndex * pageLength < dataToShow.length
      ? pageIndex * pageLength
      : dataToShow.length;
  for (
    var index = (pageIndex - 1) * pageLength;
    index < pageIndex * pageLength && index < dataToShow.length;
    index++
  ) {
    if (
      index >= (pageIndex - 1) * pageLength &&
      index < pageIndex * pageLength
    ) {
      const newRow = document.createElement("tr");
      const name = document.createElement("td");
      const position = document.createElement("td");
      const office = document.createElement("td");
      const age = document.createElement("td");
      const startDate = document.createElement("td");
      const salary = document.createElement("td");

      age.style.textAlign = "right";
      startDate.style.textAlign = "right";
      salary.style.textAlign = "right";

      name.textContent = dataToShow[index].name;
      position.textContent = dataToShow[index].position;
      office.textContent = dataToShow[index].office;
      age.textContent = dataToShow[index].age;
      const dateParts = dataToShow[index].startDate.split("-");
      startDate.textContent =
        Number(dateParts[1]) + "/" + Number(dateParts[2]) + "/" + dateParts[0];
      salary.textContent = dataToShow[index].salary;

      newRow.appendChild(name);
      newRow.appendChild(position);
      newRow.appendChild(office);
      newRow.appendChild(age);
      newRow.appendChild(startDate);
      newRow.appendChild(salary);
      if ((index + 1) % 2 == 0) {
        newRow.classList.add("even");
      } else {
        newRow.classList.add("odd");
      }
      table.appendChild(newRow);
    }
  }
}

// Pagination

function generatePageButton() {
  numberOfPage = Math.ceil(dataToShow.length / pageLength);
  while (pageButtonWrapper.firstChild) {
    pageButtonWrapper.removeChild(pageButtonWrapper.firstChild);
  }
  for (var i = 1; i <= numberOfPage; i++) {
    const pageButton = document.createElement("a");
    if (i == 1) {
      pageButton.classList.add("chosen-button");
      previousButton.classList.remove("page-button");
      nextButton.classList.add("page-button");
    }
    if (numberOfPage == 1) {
      previousButton.classList.remove("page-button");
      nextButton.classList.remove("page-button");
    }
    pageButton.classList.add("page-button", "page-number");
    pageButton.textContent = i;
    pageButton.addEventListener("click", () => {
      pageIndex = Number(pageButton.textContent);
      if (pageIndex == 1) {
        previousButton.classList.remove("page-button");
        nextButton.classList.add("page-button");
      } else if (pageIndex == numberOfPage) {
        previousButton.classList.add("page-button");
        nextButton.classList.remove("page-button");
      } else {
        previousButton.classList.add("page-button");
        nextButton.classList.add("page-button");
      }
      if (numberOfPage == 1) {
        previousButton.classList.remove("page-button");
        nextButton.classList.remove("page-button");
      }
      const pageButtons = document.querySelectorAll(".page-number");
      pageButtons.forEach((button) => {
        button.classList.remove("chosen-button");
      });
      pageButton.classList.add("chosen-button");
      showintable();
    });
    pageButtonWrapper.appendChild(pageButton);
  }
}

previousButton.addEventListener("click", () => {
  if (pageIndex != 1) {
    pageIndex--;
    if (pageIndex == 1) {
      previousButton.classList.remove("page-button");
      nextButton.classList.add("page-button");
    } else {
      previousButton.classList.add("page-button");
      nextButton.classList.add("page-button");
    }
    refreshNumberButton();
  }
});

nextButton.addEventListener("click", () => {
  if (pageIndex != numberOfPage) {
    pageIndex++;
    if (pageIndex == numberOfPage) {
      previousButton.classList.add("page-button");
      nextButton.classList.remove("page-button");
    } else {
      previousButton.classList.add("page-button");
      nextButton.classList.add("page-button");
    }
    refreshNumberButton();
  }
});

function refreshNumberButton() {
  const pageButtons = document.querySelectorAll(".page-number");
  pageButtons.forEach((button) => {
    button.classList.remove("chosen-button");
  });
  pageButtons[pageIndex - 1].classList.add("chosen-button");
  showintable(dataToShow);
}

const lengthSelect = document.querySelector(".table-length-select");
lengthSelect.addEventListener("change", () => {
  pageLength = Number(lengthSelect.value);
  pageIndex = 1;
  showintable();
  generatePageButton();
});

// Sort
sortFields.forEach((sortField) => {
  sortField.addEventListener("click", () => {
    const fieldAscending = sortField.querySelector(".ascending");
    const fieldDescending = sortField.querySelector(".descending");
    if (sortField.childNodes[0].textContent.trim() == sortFieldText) {
      if (fieldAscending.style.opacity == "0.6") {
        fieldDescending.style.opacity = "0.6";
        fieldAscending.style.opacity = "0.125";
        order = 0;
      } else {
        fieldDescending.style.opacity = "0.125";
        fieldAscending.style.opacity = "0.6";
        order = 1;
      }
    } else {
      sortFieldText = sortField.childNodes[0].textContent.trim();
      const ascendingItems = document.querySelectorAll(".ascending");
      const descendingItems = document.querySelectorAll(".descending");
      ascendingItems.forEach((ascendingItem) => {
        ascendingItem.style.opacity = "0.125";
      });
      descendingItems.forEach((descendingItem) => {
        descendingItem.style.opacity = "0.125";
      });
      fieldAscending.style.opacity = "0.6";
      order = 1;
    }
    classifySortData();
    setSortingColumnColor();
  });
});

function classifySortData() {
  if (sortFieldText == "Name") {
    sortData("name");
  } else if (sortFieldText == "Position") {
    sortData("position");
  } else if (sortFieldText == "Office") {
    sortData("office");
  } else if (sortFieldText == "Age") {
    sortData("age");
  } else if (sortFieldText == "Start date") {
    sortData("startDate");
  } else {
    sortData("salary");
  }
  showintable();
}

function sortData(key) {
  if (key != "salary") {
    tableData.sort((a, b) => {
      if ((a[key] > b[key] && order) || (a[key] < b[key] && !order)) {
        return 1;
      } else if (a[key] == b[key]) {
        return 0;
      } else {
        return -1;
      }
    });
  } else {
    tableData.sort((a, b) => {
      if (
        (toNumber(a[key]) > toNumber(b[key]) && order) ||
        (toNumber(a[key]) < toNumber(b[key]) && !order)
      ) {
        return 1;
      } else if (toNumber(a[key]) == toNumber(b[key])) {
        return 0;
      } else {
        return -1;
      }
    });
  }
  dataToShow = tableData.map((obj) => ({ ...obj }));
}

function toNumber(a) {
  const takeNum = a.slice(1);
  const numParts = takeNum.split(",");
  return Number(
    numParts.reduce((pre, curr, index) => {
      return pre + curr;
    })
  );
}

// Search
const searchInput = document.querySelector(".search-input");
searchInput.addEventListener("input", () => {
  dataToShow.length = 0;
  const searchText = searchInput.value;
  totalFilteredInfo.innerHTML = "";
  if (searchText == "") {
    dataToShow = tableData.map((obj) => ({ ...obj }));
  } else {
    tableData.forEach((data) => {
      const keys = Object.keys(data);
      for (var i = 0; i < keys.length; i++) {
        if (data[keys[i]].toLowerCase().includes(searchText.toLowerCase())) {
          dataToShow.push({ ...data });
          break;
        }
      }
    });
    if (dataToShow.length != tableData.length) {
      totalFilteredInfo.innerHTML =
        "(filtered from " + tableData.length + " total entries)";
    }
  }
  totalInfo.innerHTML = dataToShow.length;
  showintable();
  generatePageButton();
});

// add color for column when sorting following column value

function setSortingColumnColor() {
  var indexOfSort;
  for (var i = 0; i < sortFields.length; i++) {
    if (sortFields[i].childNodes[0].textContent.trim() == sortFieldText) {
      indexOfSort = i;
      break;
    }
  }
  const cells = document.querySelectorAll("tbody > tr > td");
  cells.forEach((cell) => {
    cell.classList.remove("odd-chosen-column");
    cell.classList.remove("even-chosen-column");
  });
  const odds = document.querySelectorAll(".odd");
  const evens = document.querySelectorAll(".even");
  odds.forEach((odd) => {
    odd.querySelectorAll("td")[indexOfSort].classList.add("odd-chosen-column");
  });
  evens.forEach((even) => {
    even
      .querySelectorAll("td")
      [indexOfSort].classList.add("even-chosen-column");
  });
}

// const indexOfSort = Array.from(sortFields).indexOf(sortField);
